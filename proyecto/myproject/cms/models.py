from django.db import models


# Create your models here.
class Counter(models.Model):
    count = models.IntegerField(default=0)

    @classmethod
    def get_or_create_counter(cls):
        counter, created = cls.objects.get_or_create(pk=1)
        return counter
class Contenido(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField()
