from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido, Counter


@csrf_exempt
def index(request):
    if request.method == "POST":
        value = request.POST.get('valor')
        short = request.POST.get('short', None)
        if not value:
            response = "Invalid URL, enter a correct url"
            return HttpResponse(response)
        prefix = 'https://'
        if not value.startswith('http://') and not value.startswith('https://'):
            value = prefix + value
        list_values = Contenido.objects.values_list('valor', flat=True)
        if value in list_values:
            content_list = Contenido.objects.all()
            template = loader.get_template('cms/index.html')
            context = {
                'content_list': content_list
            }
            return HttpResponse(template.render(context, request))
        else:
            counter = Counter.get_or_create_counter()
            if short is None or short.strip() == '':
                short = counter.count
                counter.count += 1
                counter.save()
                content = Contenido(clave=short, valor=value)
                content.save()
                content_list = Contenido.objects.all()
                template = loader.get_template('cms/index.html')
                context = {
                    'content_list': content_list
                }
                return HttpResponse(template.render(context, request))
            else:
                content = Contenido(clave=short, valor=value)
                content.save()
                content_list = Contenido.objects.all()
                template = loader.get_template('cms/index.html')
                context = {
                    'content_list': content_list
                }
                return HttpResponse(template.render(context, request))

    else:
        content_list = Contenido.objects.all()
        template = loader.get_template('cms/index.html')
        context = {
            'content_list': content_list
        }
        return HttpResponse(template.render(context, request))

@csrf_exempt
def get_content(request, key):
    try:
        content = Contenido.objects.get(clave=key)
        return HttpResponseRedirect(content.valor)
    except Contenido.DoesNotExist:
        response = "Not found: " + str(key)
        return HttpResponseNotFound(response)


